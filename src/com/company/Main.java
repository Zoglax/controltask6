package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n;

        n=scanner.nextInt();

        if(n%3==1)
        {
            System.out.println("VGC");
        }
        else if(n%3==2)
        {
            System.out.println("CVG");
        }
        else
        {
            System.out.println("GCV");
        }
    }
}